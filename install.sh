#!/usr/bin/env bash

pip install --upgrade pip
pip install jupyterlab ipympl

jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter lab build

echo 'Creating a notebook configuration file ...'
jupyter notebook --generate-config

echo 'Setting password for the jupyter notebook ...'
jupyter notebook password

echo 'Creating SSL certificates'
./create_key
echo 'Please modify the jupyter config file to refer to the generated ssl keys ...'


echo c.NotebookApp.certfile = u\"$HOME/.jupyter/cert.pem\" >> ~/.jupyter/jupyter_notebook_config.py
echo c.NotebookApp.keyfile = u\"$HOME/.jupyter/key.key\" >> ~/.jupyter/jupyter_notebook_config.py

echo c.NotebookApp.ip = \"*\" >> ~/.jupyter/jupyter_notebook_config.py

cat ~/.jupyter/jupyter_notebook_config.json | jq -r '.NotebookApp.password' | \
	xargs -I {} echo c.NotebookApp.password = u\'{}\' >> ~/.jupyter/jupyter_notebook_config.py

echo c.NotebookApp.open_browser = False >> ~/.jupyter/jupyter_notebook_config.py
echo c.NotebookApp.port = 3000 >> ~/.jupyter/jupyter_notebook_config.py

